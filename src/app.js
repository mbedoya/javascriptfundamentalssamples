"use strict";

//Execution Context ---------------------------------------------------------------------
var message = "Hey!";

function c() {
    message = "I am 'c' function";
    console.log(message);

    function d() {
        message = "I am 'd'";
        console.log(message);
    }

    d();
}

function b() {
    console.log(message);
    c();
    console.log(message);
}

function a() {
    var message = "I am 'a' function";
    b();
    console.log(message);
}

a();

//Expressions evaluation ----------------------------------------------------------------

console.log(2.0 == "2" == new Boolean(true) == "1");


//Objects -------------------------------------------------------------------------------
var person = new Object();

//Primitive Properties
person["firstName"] = "John";
person["lastName"] = "Wilde";

//Object Property
person["address"] = {
    city: "Melbourne",
    country: "Australia"
};

//Function Property 
person["fullName"] = function () {
    return this.firstName + " " + this.lastName;
}

console.log(person.fullName());
console.log(Object.keys(person));


//Sincronization - Threads --------------------------------------------------------------

window.addEventListener("click", () => {
    console.log("clicked");
});

window.addEventListener("load", () => {

    console.log("execution started");

    function waitFiveSeconds() {
        let milisecondsToBeReached = new Date().getTime() + 5000;
        while (new Date().getTime() < milisecondsToBeReached) { }
        console.log('wait finished');
    }
    waitFiveSeconds();

    console.log("execution finished");

});

// Global Context -----------------------------------------------------------------------

var iAmAWindowProperty = "i am a cool window property!";

var iAmANiceObject = {
    functionProperty: "hey, i am inside another context!",
    callMe: function () {
        console.log(this);
        console.log(this.functionProperty);
    }
}

iAmANiceObject.callMe();
console.log("nice object has functionProperty:" + iAmANiceObject.hasOwnProperty("functionProperty"));
//console.log(iAmANiceObject.keys());

// Hoisting -----------------------------------------------------------------------------

console.log(iAmALateAssigned);
var iAmALateAssigned = 3;

iAmDeclaredAtTheEnd();
function iAmDeclaredAtTheEnd() {
    console.log("I am at the end");
}

// Classes ------------------------------------------------------------------------------

var Car = function (year) {
    this.year = year;
    console.log("car created in " + this.year);

    this.showYear = function () {
        console.log(this.year);
    }
}

// Prototypes

var bmw = {};
Car.call(bmw, 2015);
bmw.__proto__ = Car; // This should not be used, only for samples

var mercedez = {};
Car.call(mercedez, 2017);
mercedez.__proto__ = Car; // // This should not be used, only for samples

bmw.showYear();
mercedez.showYear();

var renault = new Car(2000);
renault.showYear();


// ES6 Classes
class Person {

    constructor(name) {
        this.name = name;
    }
}

var coolPerson = new Person('John');
console.log(coolPerson.name);

class Employee extends Person {

    constructor(name, skills) {
        super(name);
        this.skills = skills;
    }

    whoIAm() {
        console.log("I am " + this.name + ", i have these skills: " + this.skills.join());
    }
}

var happyWorker = new Employee('Billy', ['Communication', 'Passion']);
console.log(happyWorker.whoIAm());

// Execution Order ----------------------------------------------------------------------

var obj2 = {
    secondFunction: function () {
        console.log("This is the second function");
    }
}

var obj1 = {
    firstFunction: function () {
        console.log("before 2nd function");
        obj2.secondFunction();
        console.log("after 2nd function");
    }
}

obj1.firstFunction();


// Vars Scope

var religion = "Catholic";

var PersonObject = function(name){
    this.name = name;

    this.howIAm = function () {
        return "This is " + this.name + ", i am " + religion;
    }
}

var religiousPerson = new PersonObject('Andrew');
console.log(religiousPerson.howIAm());

// Third Party APIs

navigator.geolocation.getCurrentPosition(function (position) {
    console.log(position)
});

// Katas --------------------------------------------------------------------------------

// Readable hours

http://www.codewars.com/kata/52685f7382004e774f0001f7/train/javascript

var humanReadable = function(seconds) {

    var formatNumber = function (number) {
        return number.toString().length == 1 ? "0" + number : number;
    }

    var hours = Math.floor(seconds / 3600);
    var remainingSeconds = seconds - hours * 3600;
    var minutes = Math.floor(remainingSeconds / 60);
    remainingSeconds = seconds - hours * 3600 - minutes * 60;
    seconds = remainingSeconds;

    return formatNumber(hours) + ":" + formatNumber(minutes) + ":" + formatNumber(seconds);
}

// Sum Strings as Numbers

function sumStrings(a, b) {
    return parseInt(Array.from(arguments).map(Number).reduce((n, acc) => n + acc)).toString();
}

// Tribonacci - https://www.codewars.com/kata/tribonacci-sequence

function tribonacci(signature, n) {

    if (n <= signature.length) {
        return signature.slice(0, n);
    }

    var generateNumber = (array) => {
        if (array.length < n) {
            if (array.length < 3) {
                array.unshift(0);
                return generateNumber(array);
            } else {
                const nextNumber = array.slice(-3).reduce((n, acc) => acc + n, 0);
                array.push(nextNumber);
                return generateNumber(array);
            }
        } else {
            return array;
        }
    }

    return generateNumber(signature);
}

console.log(tribonacci([1, 0, 1], 6));



// Sudoku -------------------------------------------------------------------------------

function validSolution(board) {

    var validElements = "123456789";

    var groupIsCorrect = function (array) {
        return array.slice().sort().join("") === validElements;
    }

    var rowsAreValid = function () {
        return board
            .map((row) => groupIsCorrect(row))
            .filter((validRow) => validRow).length === 9
    }

    var colsAreValid = function () {
        for (let i = 0; i < 9; i++) {
            if (!groupIsCorrect(board.map(row => row[i]))) {
                return false;
            }
        }
        return true;
    }

    var blocksAreValid = function () {
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (
                    !groupIsCorrect(
                        board[i * 3]
                            .slice(j * 3, j * 3 + 3)
                            .concat(
                            board[i * 3 + 1].slice(j * 3, j * 3 + 3),
                            board[i * 3 + 2].slice(j * 3, j * 3 + 3)
                            )
                    )
                ) {
                    return false;
                }
            }
        }

        return true;
    }

    return rowsAreValid() && colsAreValid() && blocksAreValid();
}